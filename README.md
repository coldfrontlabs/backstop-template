# Backstop Test Template

## Environment Variables

The following are required to be present in your environment variables so the
script can be run properly:

- CI_BACKSTOP_USER: Username to pass to ``drush uli`` to login to the application
- CI_BACKSTOP_REF_ALIAS: Drush site alias to build the reference test against (withouth the @ symbol)
- CI_BACKSTOP_TEST_ALIAS: Drush site alias to test (without the @ symbol)
- CI_BACKSTOP_REF_HOST: Reference site hostname. Used in tests to general URLs.
- CI_BACKSTOP_TEST_HOST: Test site hostname. Used in tests to general URLs.

E.g. Sample GitLab CI config

```yml
# @todo backstop tests
.css_regression_template: &css_regression # Standard css regression test template.
  environment:
    <<: *deploy_drupal_env
  script:
    # Copy the site's aliase file so we can use any generated aliases.
    - cp -r $CI_PROJECT_NAME-build/drush/site-aliases/*.aliases.drushrc.php drush/site-aliases/
    - drush cc drush
    - export CI_BACKSTOP_REF_HOST=$(drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" sa --fields=uri --field-labels=0 --format=list @$CI_BACKSTOP_REF_ALIAS)
    - export CI_BACKSTOP_TEST_HOST=$(drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" sa --fields=uri --field-labels=0 --format=list @$CI_BACKSTOP_TEST_ALIAS)
    - echo $CI_BACKSTOP_REF_HOST
    - echo $CI_BACKSTOP_TEST_HOST
    - echo $CI_BACKSTOP_REF_ALIAS
    - echo $CI_BACKSTOP_TEST_ALIAS
    - echo $CI_BACKSTOP_USER
    - drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options='$DRUSH_SSH_OPTIONS' @$CI_BACKSTOP_TEST_ALIAS status
    - npm run backstop || true
    - drush $DRUSH_SITE_ALIAS_OPTIONS --ssh-options="$DRUSH_SSH_OPTIONS" rsync backstop_data/ @$DRUSH_ALIAS_GROUP.$DRUSH_ALIAS_TYPE:backstop_data
    - echo $CI_BACKSTOP_TEST_HOST/backstop_data/html_report
  artifacts:
    name: "$CI_PROJECT_NAME-$CI_BUILD_REF_NAME-backstop_data"
    expire_in: 2 days
    when: always
    paths:
      - backstop_data


## Dev validate CSS
validate_feat_css:
  <<: *css_regression
  stage: validate
  variables:
    <<: *deploy_drupal_vars
    CI_BACKSTOP_USER: mytestuser
    DRUSH_ALIAS_TYPE: $CI_COMMIT_REF_SLUG
    CI_BACKSTOP_REF_ALIAS: $DRUSH_ALIAS_GROUP.dev
    CI_BACKSTOP_TEST_ALIAS: $DRUSH_ALIAS_GROUP.$CI_COMMIT_REF_SLUG
  dependencies:
    - drush_make_dev_build
  only:
    - branches
  except:
    - 7.x-1.x
    - tags
  tags:
    - drupal 7
    - development

```

## Running tests
``npm run backstop`` to do the full test suite.